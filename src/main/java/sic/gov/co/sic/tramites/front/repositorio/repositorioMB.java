package sic.gov.co.sic.tramites.front.repositorio;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import sic.gov.co.sic.tramites.SicPersona;
import sic.gov.co.sic.tramites.SicTramite;
import sic.gov.co.sic.tramites.SicTramitePersona;
import sic.gov.co.sic.tramites.SicEmpreados;

/**
 * <b>Descripción:</b> Repositorio encargado de manejar las peticiones HTTP al servicio REST 
 * 
 * <b>Proyecto:</b> 
 *
 * @author Ing.Jorge Serrano <jaserranoj@unal.edu.co>
 * @version 1.0
 */ 

public class repositorioMB  {
    
    // Atributos
    private static String urlBase;
    
    private static String urlBackend="http://localhost:7001/sic-tramites-ws/webresources/";
    
     private static String URI_LOGIN= urlBackend + "sicempreados/consultar/{id}/{td}";
    /** Constante que determina la URI del servicio REST de Tramites */
    
    /** Atributo que determina el cliente HTTP */
    Client client = ClientBuilder.newClient();
    
    public List<SicEmpreados> consultarCiudadano(String usuario, String contrasena) throws Exception {
        return client.target(URI_LOGIN)
                 .resolveTemplate("id", usuario)
                .resolveTemplate("td", contrasena)
                .queryParam("verbose", true)
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<SicEmpreados>>() {
                });
    }
    
    
    public List<SicTramite> consultarTramites() throws Exception {
        return client.target(urlBackend + "sictramite")
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<SicTramite>>(){});
    }
    
     public SicTramite consultarTramite(String id) throws Exception {
        return client.target(urlBackend + "sictramite/"+id)
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<SicTramite>(){});
    }
     
      public SicEmpreados consultarFuncionario(String id) throws Exception {
        return client.target(urlBackend + "sicempreados/"+id)
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<SicEmpreados>(){});
    }
    
    public List<SicPersona> consultarPersonas() throws Exception {
        return client.target(urlBackend + "sicpersona")
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<SicPersona>>(){});
    }
    
    public List<SicTramitePersona> consultarTramitesPersonas() throws Exception {
        return client.target(urlBackend + "sictramitepersona")
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<SicTramitePersona>>(){});
    }
    
     public String contarPersonas() throws Exception {
        return client.target(urlBackend + "sicpersona/count")
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<String>(){});
    }
    
     
     public SicPersona crearTercero(SicPersona tercero) throws Exception {
        Response response = client.target(urlBackend + "sicpersona")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(tercero, MediaType.APPLICATION_JSON));
            
        // Validación
        if (response.getStatusInfo().getStatusCode() >= 200 && response.getStatusInfo().getStatusCode() <= 300) {
            return tercero;
        }
        return null;
    }
     
     public SicTramitePersona crearTramite(SicTramitePersona tramite) throws Exception {
        Response response = client.target(urlBackend + "sictramitepersona")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(tramite, MediaType.APPLICATION_JSON));
            
        // Validación
        if (response.getStatusInfo().getStatusCode() >= 200 && response.getStatusInfo().getStatusCode() <= 300) {
            return tramite;
        }
        return null;
    }
}
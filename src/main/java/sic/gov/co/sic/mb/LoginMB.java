/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package sic.gov.co.sic.mb;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import sic.gov.co.sic.tramites.SicEmpreados;
import sic.gov.co.sic.tramites.front.repositorio.repositorioMB;

/**
 *
 * @author ingeniun
 */
@Named
@SessionScoped
public class LoginMB implements Serializable {

    private String usuario;
    private String contraseña;
    private boolean login = true;
    private final repositorioMB repositorioSIC = new repositorioMB();
    private List<SicEmpreados> consultaLogin = new ArrayList<>();

    ;
    /**
     * Creates a new instance of LoginMB
     */
    public LoginMB() {

    }

    public String validarLogin() {
        String destino;

        if (login) {

            destino = "inicio.xhtml?faces-redirect=true";
        } else {

            destino = "login.xhtml?faces-redirect=true";

        }

        return destino;
    }

    public boolean validarUsuario() {

        try {
            consultaLogin = repositorioSIC.consultarCiudadano(usuario, contraseña);
        } catch (Exception ex) {
            Logger.getLogger(LoginMB.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (!consultaLogin.isEmpty()) {
            return true;
        } else {
            return false;
        }

    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public boolean isLogin() {
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

    public List<SicEmpreados> getConsultaLogin() {
        return consultaLogin;
    }

    public void setConsultaLogin(List<SicEmpreados> consultaLogin) {
        this.consultaLogin = consultaLogin;
    }

}

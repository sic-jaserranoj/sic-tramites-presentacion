/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package sic.gov.co.sic.mb;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import sic.gov.co.sic.tramites.SicEmpreados;
import sic.gov.co.sic.tramites.SicPersona;
import sic.gov.co.sic.tramites.SicTramite;
import sic.gov.co.sic.tramites.SicTramitePersona;
import sic.gov.co.sic.tramites.front.repositorio.repositorioMB;

/**
 * <b>Descripción:</b> MB encargado de controlar los tramites y operaciones del
 * servicio
 *
 * <b>Proyecto:</b>
 *
 * @author Ing.Jorge Serrano <jaserranoj@unal.edu.co>
 * @version 1.0
 */
@Named
@SessionScoped
public class TramitesManagedBean implements Serializable {

    private List<SicTramite> tramitesSic = new ArrayList<>();
    private final repositorioMB repositorioSIC = new repositorioMB();
    private SicTramite tramiteSeleccionado;
    private String idTramiteSeleccionado;
    private SicTramitePersona crearTramite;
    private SicPersona crearTercero;
    private SicEmpreados usuarioLogin;
    
    /**
     * Creates a new instance of TramitesManagedBean
     */
    public TramitesManagedBean() {
        inicializar();
    }

    @PostConstruct
    public void init() {
        consultarTramites();
    }

    public List<SicTramite> getTramitesSic() {
        return tramitesSic;
    }

    public void setTramitesSic(List<SicTramite> tramitesSic) {
        this.tramitesSic = tramitesSic;
    }

    private void consultarTramites() {
        try {
            tramitesSic = repositorioSIC.consultarTramites();
        } catch (Exception ex) {
            Logger.getLogger(TramitesManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String guardarTercero() {

        try {
            repositorioSIC.consultarPersonas();
            int insp_sec = repositorioSIC.consultarPersonas().size();
            insp_sec++;
            BigDecimal persona_id = new BigDecimal(insp_sec);
            crearTercero.setIdPersona(persona_id);
            repositorioSIC.crearTercero(crearTercero);
        } catch (Exception ex) {
            Logger.getLogger(TramitesManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String destino;

        destino = "tramite.xhtml?faces-redirect=true";

        return destino;
    }

    public String guardarTramite() {

         try {
           
           tramiteSeleccionado=repositorioSIC.consultarTramite(idTramiteSeleccionado);
   
        } catch (Exception ex) {
            Logger.getLogger(TramitesManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            crearTramite.setIdTramitePersona(generarSecuencia(repositorioSIC.consultarTramitesPersonas().size()));
            crearTramite.setFechaRadicado(new Date());
            crearTramite.setSicPersonaIdPersona(crearTercero);
            crearTramite.setSicTramiteIdTramite(tramiteSeleccionado);
            crearTramite.setSicEmpreadosIdEmpleado(repositorioSIC.consultarFuncionario("1"));
            repositorioSIC.crearTramite(crearTramite);
        } catch (Exception ex) {
            Logger.getLogger(TramitesManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        String destino;

        destino = "confirmacion.xhtml?faces-redirect=true";

        return destino;
    }
    
    public String continuarTramite() {
        String destino;

        if (idTramiteSeleccionado == null) {
            FacesContext.getCurrentInstance().addMessage("notificar_usuario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "En número de documento no es válida.", "Seleccione el trámite." ));
            destino = "inicio.xhtml?faces-redirect=true";
        } else {

            destino = "tercero.xhtml?faces-redirect=true";

        }

        return destino;
    }
    
    public String salir() {
        String destino;

        inicializar();

        destino = "inicio.xhtml?faces-redirect=true";

        

        return destino;
    }
    
   

    public BigDecimal generarSecuencia(int actual) {

        actual++;
        return new BigDecimal(actual);

    }
   

    public SicTramite getTramiteSeleccionado() {
        return tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(SicTramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public SicTramitePersona getCrearTramite() {
        return crearTramite;
    }

    public void setCrearTramite(SicTramitePersona crearTramite) {
        this.crearTramite = crearTramite;
    }

    public SicPersona getCrearTercero() {
        return crearTercero;
    }

    public void setCrearTercero(SicPersona crearTercero) {
        this.crearTercero = crearTercero;
    }

    public void inicializar() {
        crearTramite = new SicTramitePersona();
        crearTercero = new SicPersona();
        tramiteSeleccionado = new SicTramite();
    }

    public String getIdTramiteSeleccionado() {
        return idTramiteSeleccionado;
    }

    public void setIdTramiteSeleccionado(String idTramiteSeleccionado) {
        this.idTramiteSeleccionado = idTramiteSeleccionado;
    }

    public SicEmpreados getUsuarioLogin() {
        return usuarioLogin;
    }

    public void setUsuarioLogin(SicEmpreados usuarioLogin) {
        this.usuarioLogin = usuarioLogin;
    }
    
     public String getfechaDetallada(Date fecha) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        return dateFormat.format(fecha);
    }
}
